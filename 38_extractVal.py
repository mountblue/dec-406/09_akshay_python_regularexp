#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:27:57 2018

Description : Write a Python program to extract values between
quotation marks of a string.

@author: akshay
"""
import re
string = input()
string = re.findall(r'"(.*?)"',string)
print(string)
