#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 09:48:17 2018

Description : Write a Python program to find all five characters long word in a string.

@author: akshay
"""
import re
string = input()
string = re.findall(r'\b[A-Za-z]{5}\b',string)
print(string)