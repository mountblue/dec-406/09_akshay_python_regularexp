#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 14:20:48 2018

Description :  Write a Python program to remove words 
from a string of length between 1 and a given number.

@author: akshay
"""
import re
string = input()
number = int(input())
string = re.sub(r'.*','',string,count = number)
print(string)
