#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 15:51:49 2018

Description : Write a Python program to remove the
 parenthesis area in a string.

@author: akshay
"""
import re
string = input()
string = re.finditer(r'\([.*]\)',string)
for i in string:
    print(i.group())