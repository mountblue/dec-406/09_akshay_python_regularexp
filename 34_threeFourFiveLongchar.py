#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 09:56:48 2018

Description : Write a Python program to find all three, four,
five characters long words in a string.

@author: akshay
"""
import re
string = input()
string = re.findall(r'\b[a-zA-z]{3,5}\b',string)
print(string)