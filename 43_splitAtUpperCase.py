#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 11:58:06 2018

Description : Write a Python program to split a string at uppercase letters.

@author: akshay
"""
import re
string = input()
string = re.sub(r'\B[A-Z]',lambda x: ' '+x.group(0),string)
print(string)

