#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:41:34 2018

Description : Write a Python program to remove multiple spaces in a string.

@author: akshay
"""
import re
string = input()
string = re.sub(r' +',' ',string)
print(string)

