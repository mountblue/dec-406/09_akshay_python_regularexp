#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:56:07 2018

Description : Write a Python program to find urls in a string.

@author: akshay
"""
import re
string = input()
string = re.search(r"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$",string)
print(string.group())
