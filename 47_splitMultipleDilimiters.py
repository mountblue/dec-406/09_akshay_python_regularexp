#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 13:00:06 2018

Description : 47. Write a Python program to split a 
string with multiple delimiter.

@author: akshay
"""
import re
string = input()
string = re.split(r',|/*|;',string)
print(string)