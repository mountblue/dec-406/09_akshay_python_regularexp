#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 14:14:49 2018

Description : Write a Python program to check a 
decimal with a precision of 2.

@author: akshay
"""
import re
string = input()
string = re.search(r'^[0-9]+(\.[0-9]{1,2})?$',string)
print(bool(string))
