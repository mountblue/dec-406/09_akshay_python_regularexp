#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:21:49 2018

Description : Write a python program to convert snake case
 string to camel case string.

@author: akshay
"""
import re
string = input()
string = re.sub(r'_([a-z])', lambda x: x.group(1).upper(), string)
print(string)
