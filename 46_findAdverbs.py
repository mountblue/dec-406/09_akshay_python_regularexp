#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 12:38:52 2018

Description :  Write a Python program to find all adverbs 
and their positions in a given sentence.

@author: akshay
"""
import re
string = input()
string = re.finditer(r'[a-zA-Z]+ly',string)
for i in string:
    print(i.group(),i.span())