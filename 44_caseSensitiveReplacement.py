#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 12:07:41 2018

Description :  Write a Python program to do a 
case-insensitive string replacement.

@author: akshay
"""
import re
string = input("Enter string : ")
reg = input("What you want to replace : ")
repTxt = input("With : ")
reg = re.compile(reg,re.IGNORECASE)
string = reg.sub(repTxt,string)
print(string)
