#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:04:12 2018

Description : Write a Python program to find all words 
which are at least 4 characters long in a string.

@author: akshay
"""
import re 
string = input()
string = re.findall(r'\b[a-zA-Z]{4,}\b',string)
print(string)
