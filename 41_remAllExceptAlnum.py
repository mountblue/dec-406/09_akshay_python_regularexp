#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:49:13 2018

Description : Write a Python program to remove everything
 except alphanumeric characters from a string.

@author: akshay
"""
import re
string = input()
string = re.sub(r'_|[^\w]+','',string)
print(string)
