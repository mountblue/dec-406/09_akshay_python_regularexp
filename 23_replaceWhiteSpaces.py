#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 28 14:09:46 2018

@author: akshay
"""
import re
subs = {' ': '_', '_':' '}
string = input('Enter string: ')
string = re.sub(r'( )|(_)',lambda x:subs[x.group()],string)
print(string)
