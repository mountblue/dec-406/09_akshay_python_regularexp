#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:07:21 2018

Description : Write a python program to convert camel case string
 to snake case string.

@author: akshay
"""
import re
string = input()
string = re.sub(r'[A-Z]', lambda x: '_' + x.group(0).lower(), string)
print(string)
