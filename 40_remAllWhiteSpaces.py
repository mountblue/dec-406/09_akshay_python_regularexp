#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 10:44:12 2018

Description : Write a Python program to remove all whitespaces from a string.

@author: akshay
"""
import re
string = input()
string = re.sub(r' +','',string)
print(string)